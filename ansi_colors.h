// ansi_colors.h
// Program to set text coloring more conveniently.

#ifndef ANSI_COLORS
#define ANSI_COLORS

#include <stdio.h>

void red ()
{
  printf("\033[1;31m");
}

void yellow ()
{
  printf("\033[1;33m");
}

void green ()
{
  printf("\033[0;32m");
}

void blue()
{
  printf("\033[0;34m");
}

void cyan()
{
  printf("\033[0;36m");
}

void purple()
{
  printf("\033[0;35m");
}

void white()
{
  printf("\033[0;37m");
}

void reset()
{
  printf("\033[0m");
}

#endif
