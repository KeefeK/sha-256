// sha-256 program
// Author: Kayleigh Keefe

///////////////////////////////////////////////////////////////////////////////////////
//===================================================================================//
//===============================  REFERENCES  ======================================//
//===================================================================================//
///////////////////////////////////////////////////////////////////////////////////////
// C data types: https://en.wikipedia.org/wiki/C_data_types                          //
// SHA-256 pseudocode: https://en.wikipedia.org/wiki/SHA-2                           //
///////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////
//                                                                                   //
//              HOW TO CONVERT A FLOATING POINT VALUE TO HEXADECIMAL                 //
//                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////
//To convert the fractional part to base 16, use successive                          //
// multiplication by 16.                                                             //
// Suppose you want to convert say 0.12.                                             //
// Take 0.12 x 16 ; this equals 1.92. The integer part, converted to hex,            //
// becomes your next hex digit and the fractional part is again multiplied           //
// by 16. Repeat this until the fractional part is zero, or you have                 //
// enough digits.                                                                    //
// Thus                                                                              //
// 0.12 x 16 = 1.92 translation so far .1                                            //
// ..92 x 16 = 14.72 .1E                                                             //
// ..72 x 16 = 11.52 .1EB                                                            //
// ..52 x 16 = 8.32 .1EB8                                                            //
// ..32 x 16 = 5.12 .1EB85                                                           //
// ..12 x 16 = 1.92 so now it starts to repeat. So your                              //
// translated number will be .1EB851EB851EB85... in hexadecimal.                     //
///////////////////////////////////////////////////////////////////////////////////////

#include "ansi_colors.h"
#include <limits.h> // For max constants
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h> // For malloc
#include <string.h>
#include <time.h>

/*
uint32_t rightrotate(uint32_t value, int num_rot)
{
  int i; // rotation index
  for(i=0; i<num_rot; i=i + 1)
  {

  }
}
*/


// Function: Least Significant Byte
int LSB

int main(int argc, char **argv)
{
  clock_t begin = clock();
  switch(argc)
  {
    case 1:
      printf("Need an input file!\n");
      return 1;
      break;
    case 2:
      //////////////////////////////////////////////////////////////////////////////////////
      //                        Declarations for file reading                             //
      //////////////////////////////////////////////////////////////////////////////////////
      // The following print statement was inserted because the C compiler is fucking retarded.
      printf("Attempting to open file.\n");
      FILE *fp;
      // Open file for reading only.
      fp = fopen(argv[1], "r");
      if(fp == NULL) // Abort if file cannot be opened.
      {
        red();
        perror("Failed to open file.\n");
        reset();
        return 1;
      }
      else
      {
        yellow();
        printf("File opened successfully.\n");
        reset();
      }
      //////////////////////////////////////////////////////////////////////////////////////
      //            !!!!!!!!!!!!!!        IMPORTANT        !!!!!!!!!!!!!!                 //
      //////////////////////////////////////////////////////////////////////////////////////
      // FILE SIZE MAY NOT EXCEED 4,294,967,295 bytes or 4.29497 gigabytes.               //
      // Files exceeding this size will cause an overflow.                                //
      //////////////////////////////////////////////////////////////////////////////////////
      fseek(fp, 0L, SEEK_END);
      if(ftell(fp) > UINT_MAX)
      {
        red();
        perror("File is too big. Please choose a file smaller than 4.29497 GB.\n\n");
        reset();
        return 1;
      }
      // Note: declarations cannot be placed immediately after a label; labels must be followed by a statement.
      printf("Running for input file \"%s\":\n", argv[1]);

      // Ask whether debug should be turned on.
      char debug[100];
      do
      {
        printf("\nEnable debug? (Please enter 0 for \"no\" or 1 for \"yes\"):");
        scanf("%s", debug);
        printf("\n\n");
      } while((debug[0] != '0') && (debug[0] != '1')); // Use do-while to force 1 iteration.

      // The next two lines are to get the size of the file in bytes.
      fseek(fp, 0L, SEEK_END); // Go to the end of the file
      long long file_size = ftell(fp); // Grab current position; Must use 64-bit int

      if(debug[0])
      {
        printf("The detected file size is: %llu B\n", file_size);
        printf("The detected file size in hexadecimal is: %llX B \n", file_size);
        printf("The detected file size is: %llu b\n", (long long)(file_size*8));
        printf("The detected file size in hexadecimal is: %llX b\n\n", (file_size*8));
      }

      //////////////////////////////////////////////////////////////////////////////
      // Create buffer to store bytes in file.                                    //
      // Get the length of the file (in bytes). We do this by seeking the end of  //
      // the file and requesting the position. Use calloc to dynamically allocate //
      // sufficient memory from the heap and initialize all values to zero.       //
      //////////////////////////////////////////////////////////////////////////////

      // remainder_to_add is the number of zeros to add to the digest such that
      // the sum of the message, the byte 1000000, and 8 bytes representing the
      // size of the message if a multiple of 512 bits, or
      long long remainder_to_add = 512 - (((file_size + 1 + 8) * 8) % 512);
      if(debug[0])
      {
        printf("remainder to add: %llu\n\n", remainder_to_add);
      }
      long long buffer_size = ((file_size + 1 + 8) * 8) + remainder_to_add;

      // Break the 64-bit filesize integer into "byte-sized" pieces... get it?
      char append_array[8] = {0}; // Initialize array to store the size in bytes
      int i; // Initialize an iterator.
      // The following for-loop accomplishes this:
      // append_array[0] = (file_size >> 56) & 0xFF;
      // append_array[1] = (file_size >> 48) & 0xFF;
      // append_array[2] = (file_size >> 40) & 0xFF;
      // append_array[3] = (file_size >> 32) & 0xFF;
      // append_array[4] = (file_size >> 24) & 0xFF;
      // append_array[5] = (file_size >> 16) & 0xFF;
      // append_array[6] = (file_size >> 8) & 0xFF;
      // append_array[7] = file_size & 0xFF;
      for(i = 0; i < 8; i = i + 1)
      {
        append_array[i] = (((file_size*8) >> (8 * (7 - i)))) & 0xFF;
      }

      if(debug[0])
      {
        printf("\nFilesize as long long: %llu B\nFilesize as character array:\n", file_size);
        printf("Byte 1: %02hhX\n", append_array[0]);
        printf("Byte 2: %02hhX\n", append_array[1]);
        printf("Byte 3: %02hhX\n", append_array[2]);
        printf("Byte 4: %02hhX\n", append_array[3]);
        printf("Byte 5: %02hhX\n", append_array[4]);
        printf("Byte 6: %02hhX\n", append_array[5]);
        printf("Byte 7: %02hhX\n", append_array[6]);
        printf("Byte 8: %02hhX\n\n", append_array[7]);
      }

      // Initialize the buffer to store the digest;
      // Note: It is important to use calloc, because the stack isn't nearly big
      // enough (on Linux 5.9.11-3-MANJARO, it's 8192 KB).
      uint8_t* buffer_ptr;
      // int8_t* buffer_ptr;
      // char* buffer_ptr;
      long long buffer_size_B = buffer_size/8;
      printf("(buffer_size: %llu B)\n", buffer_size_B);
      buffer_ptr = (char*)calloc(buffer_size_B, 1);
      printf("sizeof(buffer_ptr): %lu\n", sizeof(buffer_ptr));

      if(debug[0])
      {
        printf("buffer_size: %llu b\n\n", buffer_size);
      }

      fseek(fp, 0L, SEEK_SET); // Seek back to the beginning of the file for streaming
      fread(buffer_ptr, 1, buffer_size, fp); // Read in file byte by byte.

      // Append the single 1 to the end of the message/file data.
      // buffer_ptr[file_size] = (int8_t)0x80;
      buffer_ptr[file_size] = (uint8_t)0x80;

      // Insert the 8 byte-representation of the file size at the end of the buffer
      buffer_ptr[buffer_size_B-1] = append_array[7];
      buffer_ptr[buffer_size_B-2] = append_array[6];
      buffer_ptr[buffer_size_B-3] = append_array[5];
      buffer_ptr[buffer_size_B-4] = append_array[4];
      buffer_ptr[buffer_size_B-5] = append_array[3];
      buffer_ptr[buffer_size_B-6] = append_array[2];
      buffer_ptr[buffer_size_B-7] = append_array[1];
      buffer_ptr[buffer_size_B-8] = append_array[0];

      if(debug[0])
      {
        int j;
        printf("The digest currently is:\n");
        for(j = 0; j < buffer_size_B; j = j + 1)
        {
          printf("%02hhX",buffer_ptr[j]);
          // printf("%c",buffer_ptr[j]);
          if(((j + 1) % 4 == 0) && ((j + 1) % 64 != 0)) // This if-statement is to divide the bytes into
                                                        // readable 4-byte chunks.
          {
            blue();
            printf("|");
            reset();
          }
          if((j + 1) % 64 == 0) // This if-statement is to separate the bytes into
                                // 64-byte (512-bit) chunks.
          {
            printf("\n");
          }
        }
        printf("\n\n");
      }

      // long long len = strlen(buffer_ptr);

      //////////////////////////////////////////////////////////////////////////////////////
      // These are the first 32 bits of the fractional parts (that means the part after   //
      // the decimal point) of the square roots of the first 8 primes 2..19               //
      //////////////////////////////////////////////////////////////////////////////////////

      // Lists of prime numbers can be found here:
      // <https://en.wikipedia.org/wiki/List_of_prime_numbers>
      // Feel free to check these following the example above
      int h0 = 0x6a09e667; // sqrt(2)= 1.41421356237, convert decimal 0.41421356237 to hex
      int h1 = 0xbb67ae85; // sqrt(3)= 1.73205080757, convert decimal 0.73205080757 to hex
      int h2 = 0x3c6ef372; // sqrt(5)= 2.2360679775, convert decimal 0.2360679775 to hex
      int h3 = 0xa54ff53a; // sqrt(7)= 2.64575131106, convert decimal 0.64575131106 to hex
      int h4 = 0x510e527f; // sqrt(11)= 3.31662479036, convert decimal 0.31662479036 to hex
      int h5 = 0x9b05688c; // sqrt(13)= 3.60555127546, convert decimal 0.60555127546 to hex
      int h6 = 0x1f83d9ab; // sqrt(17)= 4.12310562562, convert decimal 0.12310562562 to hex
      int h7 = 0x5be0cd19; // sqrt(19)= 4.35889894354, convert decimal 0.35889894354 to hex

      // Initialize array of round constants:
      // NOTE: You may collapse this (if your graphical text editor allows for it)
      // (first 32 bits of the fractional parts of the cube roots of the first 64 primes 2..311):
      int k[64] = {
          0x428a2f98, // cubed root of 2
          0x71374491, // cubed root of 3
          0xb5c0fbcf, // cubed root of 5
          0xe9b5dba5, // cubed root of 7
          0x3956c25b, // cubed root of 11
          0x59f111f1, // cubed root of 13
          0x923f82a4, // cubed root of 17
          0xab1c5ed5, // cubed root of 19
          0xd807aa98, // cubed root of 23
          0x12835b01, // cubed root of 29
          0x243185be, // cubed root of 31
          0x550c7dc3, // cubed root of 37
          0x72be5d74, // cubed root of 41
          0x80deb1fe, // cubed root of 43
          0x9bdc06a7, // cubed root of 47
          0xc19bf174, // cubed root of 53
          0xe49b69c1, // cubed root of 59
          0xefbe4786, // cubed root of 61
          0x0fc19dc6, // cubed root of 67
          0x240ca1cc, // cubed root of 71
          0x2de92c6f, // cubed root of 73
          0x4a7484aa, // cubed root of 79
          0x5cb0a9dc, // cubed root of 83
          0x76f988da, // cubed root of 89
          0x983e5152, // cubed root of 97
          0xa831c66d, // cubed root of 101
          0xb00327c8, // cubed root of 103
          0xbf597fc7, // cubed root of 107
          0xc6e00bf3, // cubed root of 109
          0xd5a79147, // cubed root of 113
          0x06ca6351, // cubed root of 127
          0x14292967, // cubed root of 131
          0x27b70a85, // cubed root of 137
          0x2e1b2138, // cubed root of 139
          0x4d2c6dfc, // cubed root of 149
          0x53380d13, // cubed root of 151
          0x650a7354, // cubed root of 157
          0x766a0abb, // cubed root of 163
          0x81c2c92e, // cubed root of 167
          0x92722c85, // cubed root of 173
          0xa2bfe8a1, // cubed root of 179
          0xa81a664b, // cubed root of 181
          0xc24b8b70, // cubed root of 191
          0xc76c51a3, // cubed root of 193
          0xd192e819, // cubed root of 197
          0xd6990624, // cubed root of 199
          0xf40e3585, // cubed root of 211
          0x106aa070, // cubed root of 223
          0x19a4c116, // cubed root of 227
          0x1e376c08, // cubed root of 229
          0x2748774c, // cubed root of 233
          0x34b0bcb5, // cubed root of 239
          0x391c0cb3, // cubed root of 241
          0x4ed8aa4a, // cubed root of 251
          0x5b9cca4f, // cubed root of 257
          0x682e6ff3, // cubed root of 263
          0x748f82ee, // cubed root of 269
          0x78a5636f, // cubed root of 271
          0x84c87814, // cubed root of 277
          0x8cc70208, // cubed root of 281
          0x90befffa, // cubed root of 283
          0xa4506ceb, // cubed root of 293
          0xbef9a3f7, // cubed root of 307
          0xc67178f2  // cubed root of 311
        };

      int num_chunks = buffer_size/512;

      if(debug[0])
      {
        printf("The number of 512-bit chunks is: %u\n\n", num_chunks);
      }

      // Process the message in successive 512-bit chunks:
      int chunk;
      for(chunk = 0; chunk < num_chunks; chunk = chunk + 1)
      {
        uint32_t w[64] = {0}; // w is an array of 32-bit integers
        // Create a 64-entry message schedule array w[0..63] of 32-bit words
        // (The initial values in w[0..63] don't matter, so many implementations zero them here)
        // copy chunk into first 16 words w[0..15] of the message schedule array
        int word_32;
        int shifted_4_mult = 0; // The multiple by which to shift forward in the array
                                // This will always be a multiple of 4.
        if(debug[0])
        {
          printf(" ______________________________________________________________________________________________________________________________________________________ \n|");
          purple();
          printf("%*chunk number: %u                                                                       ", 65, 'C', (chunk+1)); // See line 337 for info on spacing
          reset();
          printf("|\n|______________________________________________________________________________________________________________________________________________________|\n");
        }
        // For each 32-bit word
        for(word_32 = 0; word_32 < 16; word_32 = word_32 + 1)
        {
          int byte_for_32_int = 0;
          shifted_4_mult = word_32 * 4;
          for(byte_for_32_int = 0; byte_for_32_int < 4; byte_for_32_int = byte_for_32_int + 1)
          {
            w[word_32] = (buffer_ptr[byte_for_32_int + shifted_4_mult + (64*chunk)]) | (w[word_32] << 8);
            if(debug[0])
            {
              // The width field may be omitted, or a numeric integer value, or
              // a dynamic value when passed as another argument when indicated by an asterisk *.
              // Example:
              // "printf("%*d", 5, 10)" prints "   10" being printed, with a total
              // width of 5 characters.
              printf("| byte %u = %08lX; int: %*d |", byte_for_32_int, w[word_32], 10, w[word_32]);
              // printf("w[%*u], appending byte %u = %08lX ", 2, word_32, byte_for_32_int, w[word_32]);
            }
          }
          printf("\n");
        }

        if(debug[0])
        {
          printf(" ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ \n");
        }

        // Extend the first 16 words into the remaining 48 words w[16..63] of the message schedule array:

        memset(w, 0, 64); // Reset word memory for next chunk.
      }

      fclose(fp);
      free(buffer_ptr); // Return the memory back to the heap

      clock_t end = clock();
      double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
      printf("Duration: %f\n", time_spent);
      break;
    default:
      red();
      printf("Please provide only one file.\n");
      reset();
      return 1;
      break;
    }

  return 0;
}
